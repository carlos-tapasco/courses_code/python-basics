#
import urllib.request
from urllib.request import Request, urlopen

term = "televisores smart tv"
req = Request('https://www.homecenter.com.co/homecenter-co/search/?Ntt={}'.format(term.replace(" ","%20")), headers={'User-Agent': 'Mozilla/5.0'})

u = urllib.request.urlopen(req)
data = u.read()

from lxml import html
doc = html.document_fromstring(data)

for title in doc.cssselect("h2.product-title"):
    print(title.text_content())